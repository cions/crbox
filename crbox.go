// Copyright (c) 2021 cions
// Licensed under the MIT License. See LICENSE for details

package crbox

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"golang.org/x/sync/errgroup"

	"github.com/distribution/distribution/v3"
	"github.com/distribution/distribution/v3/manifest/schema2"
	"github.com/distribution/distribution/v3/reference"
	"github.com/opencontainers/go-digest"
)

var (
	ErrInvalidChecksum = errors.New("checksum verification failed")
	ErrInvalidTag      = errors.New("message authentication failed (password is wrong or data is corrupted)")
	ErrNotCrbox        = errors.New("not a crbox image")
)

type Versioned struct {
	Version uint `json:"version"`
}

type Encryptor interface {
	Encrypt(dst, src []byte) []byte
	ConfigJSON() []byte
	Overhead() int
}

type Decryptor interface {
	Decrypt(dst, src []byte) ([]byte, error)
	Verify() error
	Filename() ([]byte, error)
	Filesize() uint64
	Checksum() string
	CreatedAt() time.Time
	Overhead() int
}

type Pusher interface {
	PutChunk(context.Context, []byte) error
	Commit(context.Context, []byte) error
}

type Puller interface {
	Reference() reference.Canonical
	ConfigJSON(context.Context) ([]byte, error)
	ReadChunk(context.Context) ([]byte, error)
	NumChunks() int
}

type PushDestination struct {
	Name       reference.Named
	Repository distribution.Repository
	Tags       []string
	Config     distribution.Descriptor
	Layers     []distribution.Descriptor
}

func NewPushDestinations(ctx context.Context, references []string) ([]*PushDestination, error) {
	var repositories []string
	tagmap := make(map[string][]string)
	for _, ref := range references {
		named, err := reference.ParseNormalizedNamed(ref)
		if err != nil {
			return nil, err
		}
		named = reference.TagNameOnly(named)
		repoName := named.Name()
		if tagged, ok := named.(reference.Tagged); ok {
			if _, ok := tagmap[repoName]; !ok {
				repositories = append(repositories, repoName)
			}
			tagmap[repoName] = append(tagmap[repoName], tagged.Tag())
		} else {
			return nil, fmt.Errorf("reference must be tagged")
		}
	}

	g, ctx := errgroup.WithContext(ctx)
	destinations := make([]*PushDestination, len(repositories))
	for i, repoName := range repositories {
		i, repoName := i, repoName
		g.Go(func() error {
			ref, err := reference.WithName(repoName)
			if err != nil {
				return err
			}
			repo, err := NewRepository(ctx, ref, "push")
			if err != nil {
				return err
			}
			destinations[i] = &PushDestination{
				Name:       ref,
				Repository: repo,
				Tags:       tagmap[repoName],
			}
			return nil
		})
	}
	if err := g.Wait(); err != nil {
		return nil, err
	}

	return destinations, nil
}

func PutManifests(ctx context.Context, destinations []*PushDestination) error {
	g, ctx := errgroup.WithContext(ctx)
	for _, dst := range destinations {
		dst := dst
		g.Go(func() error {
			manifest, err := schema2.FromStruct(schema2.Manifest{
				Versioned: schema2.SchemaVersion,
				Config:    dst.Config,
				Layers:    dst.Layers,
			})
			if err != nil {
				repoName := reference.FamiliarName(dst.Name)
				return fmt.Errorf("%s: %w", repoName, err)
			}

			ms, err := dst.Repository.Manifests(ctx)
			if err != nil {
				repoName := reference.FamiliarName(dst.Name)
				return fmt.Errorf("%s: %w", repoName, err)
			}
			for _, tag := range dst.Tags {
				_, err := ms.Put(ctx, manifest, distribution.WithTag(tag))
				if err != nil {
					repoName := reference.FamiliarName(dst.Name)
					return fmt.Errorf("%s: %w", repoName, err)
				}
			}

			return nil
		})
	}
	if err := g.Wait(); err != nil {
		return err
	}

	return nil
}

func NewDecryptor(password []byte, configJSON []byte) (Decryptor, error) {
	var versioned Versioned
	if err := json.Unmarshal(configJSON, &versioned); err != nil {
		return nil, fmt.Errorf("invalid crbox.json: %w", err)
	}

	switch versioned.Version {
	case 1:
		return NewDecryptorV1(password, configJSON)
	default:
		return nil, fmt.Errorf("unsupported crbox version: %d", versioned.Version)
	}
}

func NewPuller(ctx context.Context, repo distribution.Repository, ref reference.Named) (Puller, error) {
	var dgst digest.Digest
	if digested, ok := ref.(reference.Digested); ok {
		dgst = digested.Digest()
	} else if tagged, ok := ref.(reference.Tagged); ok {
		desc, err := repo.Tags(ctx).Get(ctx, tagged.Tag())
		if err != nil {
			return nil, err
		}
		dgst = desc.Digest
	} else {
		return nil, errors.New("reference must be tagged or digested")
	}
	digested, err := reference.WithDigest(ref, dgst)
	if err != nil {
		return nil, err
	}

	ms, err := repo.Manifests(ctx)
	if err != nil {
		return nil, err
	}
	manifest, err := ms.Get(ctx, dgst)
	if err != nil {
		return nil, err
	}
	switch m := manifest.(type) {
	case *schema2.DeserializedManifest:
		return NewDockerPuller(repo, &m.Manifest, digested)
	default:
		mediaType, _, err := manifest.Payload()
		if err != nil {
			return nil, fmt.Errorf("unsupported manifest format: %w", err)
		}
		return nil, fmt.Errorf("unsupported manifest format: %s", mediaType)
	}
}
