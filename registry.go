// Copyright (c) 2021 cions
// Licensed under the MIT License. See LICENSE for details

package crbox

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/distribution/distribution/v3"
	"github.com/distribution/distribution/v3/reference"
	"github.com/distribution/distribution/v3/registry/client"
	"github.com/distribution/distribution/v3/registry/client/auth"
	"github.com/distribution/distribution/v3/registry/client/auth/challenge"
	"github.com/distribution/distribution/v3/registry/client/transport"
	"github.com/docker/cli/cli/config"
	"github.com/docker/cli/cli/config/types"
)

const (
	clientID            = "docker"
	dockerVersion       = "20.10.7"
	dockerGoVersion     = "go1.13.8"
	dockerGitCommit     = "20.10.7-0ubuntu5~20.04.2"
	dockerKernelVersion = "5.13.0-generic"
	dockerOS            = "linux"
	dockerArch          = "amd64"
)

var (
	userAgent     string
	baseTransport http.RoundTripper
)

func init() {
	elems := []string{
		fmt.Sprintf("docker/%s", dockerVersion),
		fmt.Sprintf("go/%s", dockerGoVersion),
		fmt.Sprintf("git-commit/%s", dockerGitCommit),
		fmt.Sprintf("kernel/%s", dockerKernelVersion),
		fmt.Sprintf("os/%s", dockerOS),
		fmt.Sprintf("arch/%s", dockerArch),
		fmt.Sprintf("UpstreamClient(Docker-Client/%s \\(%s\\))", dockerVersion, dockerOS),
	}
	userAgent = strings.Join(elems, " ")
}

func init() {
	baseTransport = &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout: 30 * time.Second,
		}).DialContext,
		TLSClientConfig: &tls.Config{
			MinVersion: tls.VersionTLS12,
		},
		TLSHandshakeTimeout:   15 * time.Second,
		IdleConnTimeout:       30 * time.Second,
		ResponseHeaderTimeout: 30 * time.Second,
		ForceAttemptHTTP2:     true,
	}
}

func getEndpoints(hostport string) []string {
	if hostport == "docker.io" {
		return []string{"https://registry-1.docker.io/"}
	}

	endpoints := []string{"https://" + hostport + "/"}

	host, _, err := net.SplitHostPort(hostport)
	if err != nil {
		host = hostport
	}
	ipaddrs, err := net.LookupIP(host)
	if err != nil {
		if ip := net.ParseIP(host); ip != nil {
			ipaddrs = []net.IP{ip}
		}
	}
	for _, ip := range ipaddrs {
		if ip.IsLoopback() || ip.IsPrivate() {
			endpoints = append(endpoints, "http://"+hostport+"/")
			break
		}
	}

	return endpoints
}

type staticCredentialStore types.AuthConfig

func (cs staticCredentialStore) Basic(*url.URL) (string, string) {
	return cs.Username, cs.Password
}

func (cs staticCredentialStore) RefreshToken(*url.URL, string) string {
	return cs.IdentityToken
}

func (cs staticCredentialStore) SetRefreshToken(*url.URL, string, string) {
}

func getCredentialStore(domain string) (auth.CredentialStore, error) {
	authKey := domain
	if domain == "docker.io" {
		authKey = "https://index.docker.io/v1/"
	}

	configFile, err := config.Load(config.Dir())
	if err != nil {
		return nil, err
	}
	authConfig, err := configFile.GetAuthConfig(authKey)
	if err != nil {
		return nil, err
	}
	return staticCredentialStore(authConfig), nil
}

func pingV2Registry(ctx context.Context, endpoint string, transport http.RoundTripper) (challenge.Manager, error) {
	pingClient := &http.Client{
		Transport: transport,
		Timeout:   30 * time.Second,
	}
	endpoint = strings.TrimSuffix(endpoint, "/") + "/v2/"
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, endpoint, nil)
	if err != nil {
		return nil, err
	}
	resp, err := pingClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	challengeManager := challenge.NewSimpleManager()
	if err := challengeManager.AddResponse(resp); err != nil {
		return nil, err
	}
	return challengeManager, nil
}

func NewRepository(ctx context.Context, name reference.Named, action string) (distribution.Repository, error) {
	domain := reference.Domain(name)
	remoteName, err := reference.WithName(reference.Path(name))
	if err != nil {
		return nil, err
	}
	creds, err := getCredentialStore(domain)
	if err != nil {
		return nil, err
	}

	var modifiers []transport.RequestModifier
	modifiers = append(modifiers, transport.NewHeaderRequestModifier(http.Header{
		"User-Agent": []string{userAgent},
	}))
	authTransport := transport.NewTransport(baseTransport, modifiers...)

	endpoints := getEndpoints(domain)
	for _, endpoint := range endpoints {
		challenge, err := pingV2Registry(ctx, endpoint, authTransport)
		if err != nil {
			continue
		}
		actions := []string{action}
		if action == "push" {
			actions = []string{"push", "pull"}
		}
		tokenHandler := auth.NewTokenHandlerWithOptions(auth.TokenHandlerOptions{
			Transport:   authTransport,
			Credentials: creds,
			ClientID:    clientID,
			Scopes: []auth.Scope{
				auth.RepositoryScope{
					Repository: remoteName.String(),
					Actions:    actions,
				},
			},
		})
		basicHandler := auth.NewBasicHandler(creds)
		modifiers = append(modifiers, auth.NewAuthorizer(challenge, tokenHandler, basicHandler))
		repoTransport := transport.NewTransport(baseTransport, modifiers...)
		return client.NewRepository(remoteName, endpoint, repoTransport)
	}

	return nil, fmt.Errorf("failed to connect registry %s", domain)
}
