package crbox

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"context"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/distribution/distribution/v3"
	"github.com/distribution/distribution/v3/manifest/schema2"
	"github.com/distribution/distribution/v3/reference"
	"github.com/opencontainers/go-digest"
)

var defaultEnv = []string{
	"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
}

type containerConfig struct {
	Hostname     string
	Domainname   string
	User         string
	AttachStdin  bool
	AttachStdout bool
	AttachStderr bool
	Tty          bool
	OpenStdin    bool
	StdinOnce    bool
	Env          []string
	Cmd          []string
	Image        string
	Volumes      map[string]struct{}
	WorkingDir   string
	Entrypoint   []string
	OnBuild      []string
	Labels       map[string]string
}

type rootFS struct {
	Type    string          `json:"type"`
	DiffIDs []digest.Digest `json:"diff_ids,omitempty"`
}

type history struct {
	Created    time.Time `json:"created"`
	Author     string    `json:"author,omitempty"`
	CreatedBy  string    `json:"created_by,omitempty"`
	Comment    string    `json:"comment,omitempty"`
	EmptyLayer bool      `json:"empty_layer,omitempty"`
}

type dockerImage struct {
	ID              string           `json:"id,omitempty"`
	Parent          string           `json:"parent,omitempty"`
	Comment         string           `json:"comment,omitempty"`
	Created         time.Time        `json:"created"`
	Container       string           `json:"container,omitempty"`
	ContainerConfig *containerConfig `json:"container_config,omitempty"`
	DockerVersion   string           `json:"docker_version,omitempty"`
	Author          string           `json:"author,omitempty"`
	Config          *containerConfig `json:"config,omitempty"`
	Architecture    string           `json:"architecture,omitempty"`
	Variant         string           `json:"variant,omitempty"`
	OS              string           `json:"os,omitempty"`
	Size            int64            `json:",omitempty"`
	RootFS          *rootFS          `json:"rootfs,omitempty"`
	History         []history        `json:"history,omitempty"`
	OSVersion       string           `json:"os.version,omitempty"`
	OSFeatures      []string         `json:"os.features,omitempty"`
}

func (img *dockerImage) MarshalJSON() ([]byte, error) {
	type MarshalImage dockerImage

	pass1, err := json.Marshal((*MarshalImage)(img))
	if err != nil {
		return nil, err
	}

	var c map[string]*json.RawMessage
	if err := json.Unmarshal(pass1, &c); err != nil {
		return nil, err
	}
	return json.Marshal(c)
}

type errNotFound string

func (e errNotFound) Error() string {
	return fmt.Sprintf("%s not found", string(e))
}

func fileHash(header *tar.Header, content []byte) string {
	h := sha256.New()
	fmt.Fprintf(h, "%s%s", "name", header.Name)
	fmt.Fprintf(h, "%s%d", "mode", header.Mode)
	fmt.Fprintf(h, "%s%d", "uid", header.Uid)
	fmt.Fprintf(h, "%s%d", "gid", header.Gid)
	fmt.Fprintf(h, "%s%d", "size", header.Size)
	fmt.Fprintf(h, "%s%c", "typeflag", header.Typeflag)
	fmt.Fprintf(h, "%s%s", "linkname", header.Linkname)
	fmt.Fprintf(h, "%s%s", "uname", header.Uname)
	fmt.Fprintf(h, "%s%s", "gname", header.Gname)
	fmt.Fprintf(h, "%s%d", "devmajor", header.Devmajor)
	fmt.Fprintf(h, "%s%d", "devminor", header.Devminor)
	h.Write(content)
	return fmt.Sprintf("file:%x", h.Sum(nil))
}

type dockerPusher struct {
	destinations []*PushDestination
	diffids      []digest.Digest
	history      []history
	createdAt    time.Time
	chunkCount   int
}

func NewDockerPusher(destinations []*PushDestination) (Pusher, error) {
	return &dockerPusher{
		destinations: destinations,
		createdAt:    time.Now().UTC(),
	}, nil
}

func (p *dockerPusher) createTarball(level int, filename string, content []byte) ([]byte, digest.Digest, string, error) {
	buffer := new(bytes.Buffer)
	zw, err := gzip.NewWriterLevel(buffer, level)
	if err != nil {
		return nil, "", "", err
	}
	diffid := digest.SHA256.Digester()
	tw := tar.NewWriter(io.MultiWriter(zw, diffid.Hash()))

	header := &tar.Header{
		Typeflag: tar.TypeReg,
		Name:     filename,
		Mode:     0o100644,
		Size:     int64(len(content)),
		ModTime:  p.createdAt.Truncate(time.Second),
	}
	if err := tw.WriteHeader(header); err != nil {
		return nil, "", "", err
	}
	if _, err := tw.Write(content); err != nil {
		return nil, "", "", err
	}
	filehash := fileHash(header, content)

	if err := tw.Close(); err != nil {
		return nil, "", "", err
	}
	if err := zw.Close(); err != nil {
		return nil, "", "", err
	}
	return buffer.Bytes(), diffid.Digest(), filehash, nil
}

func (p *dockerPusher) PutChunk(ctx context.Context, content []byte) error {
	p.chunkCount++
	filename := fmt.Sprintf("%06d.dat", p.chunkCount)
	payload, diffid, filehash, err := p.createTarball(gzip.NoCompression, filename, content)
	if err != nil {
		return err
	}

	action := fmt.Sprintf("Uploading Chunk #%d", p.chunkCount)
	descs, err := PutBlob(ctx, action, p.destinations, schema2.MediaTypeLayer, payload)
	if err != nil {
		return err
	}
	for i, dst := range p.destinations {
		dst.Layers = append(dst.Layers, descs[i])
	}

	p.diffids = append(p.diffids, diffid)
	p.history = append(p.history, history{
		Created:   time.Now().UTC(),
		CreatedBy: fmt.Sprintf("/bin/sh -c #(nop) COPY %s in / ", filehash),
	})

	return nil
}

func (p *dockerPusher) putHeader(ctx context.Context, configJSON []byte) error {
	payload, diffid, filehash, err := p.createTarball(gzip.DefaultCompression, "crbox.json", configJSON)
	if err != nil {
		return err
	}

	descs, err := PutBlob(ctx, "", p.destinations, schema2.MediaTypeLayer, payload)
	if err != nil {
		return err
	}
	for i, dst := range p.destinations {
		dst.Layers = append([]distribution.Descriptor{descs[i]}, dst.Layers...)
	}

	p.diffids = append([]digest.Digest{diffid}, p.diffids...)
	p.history = append([]history{
		{
			Created:   p.createdAt,
			CreatedBy: fmt.Sprintf("/bin/sh -c #(nop) COPY %s in / ", filehash),
		},
	}, p.history...)

	return nil
}

func (p *dockerPusher) putConfig(ctx context.Context) error {
	config := &dockerImage{
		Config:          &containerConfig{Env: defaultEnv},
		ContainerConfig: &containerConfig{Env: defaultEnv},
		DockerVersion:   dockerVersion,
		Architecture:    dockerArch,
		OS:              dockerOS,
		RootFS:          &rootFS{Type: "layers"},
	}

	var imageID string
	for i := 0; i < len(p.history); i++ {
		config.Config.Image = imageID
		config.ContainerConfig.Image = imageID
		config.ContainerConfig.Cmd = []string{
			"/bin/sh",
			"-c",
			strings.TrimPrefix(p.history[i].CreatedBy, "/bin/sh -c "),
		}
		config.Created = p.history[i].Created
		config.RootFS.DiffIDs = p.diffids[:i+1]
		config.History = p.history[:i+1]

		serialized, err := json.Marshal(config)
		if err != nil {
			return err
		}
		imageID = digest.SHA256.FromBytes(serialized).String()
	}

	payload, err := json.Marshal(config)
	if err != nil {
		return err
	}

	descs, err := PutBlob(ctx, "", p.destinations, schema2.MediaTypeImageConfig, payload)
	if err != nil {
		return err
	}
	for i, dst := range p.destinations {
		dst.Config = descs[i]
	}

	return nil
}

func (p *dockerPusher) Commit(ctx context.Context, configJSON []byte) error {
	if err := p.putHeader(ctx, configJSON); err != nil {
		return fmt.Errorf("putHeader: %w", err)
	}
	if err := p.putConfig(ctx); err != nil {
		return fmt.Errorf("putConfig: %w", err)
	}
	if err := PutManifests(ctx, p.destinations); err != nil {
		return fmt.Errorf("putManifest: %w", err)
	}
	return nil
}

type dockerPuller struct {
	reference  reference.Canonical
	repo       distribution.Repository
	config     distribution.Descriptor
	layers     []distribution.Descriptor
	chunkCount int
}

func NewDockerPuller(repo distribution.Repository, manifest *schema2.Manifest, ref reference.Canonical) (Puller, error) {
	return &dockerPuller{
		reference: ref,
		repo:      repo,
		config:    manifest.Layers[0],
		layers:    manifest.Layers[1:],
	}, nil
}

func (p *dockerPuller) extractTarball(filename string, payload []byte) ([]byte, error) {
	zr, err := gzip.NewReader(bytes.NewReader(payload))
	if err != nil {
		return nil, err
	}
	defer zr.Close()
	tr := tar.NewReader(zr)

	header, err := tr.Next()
	if err == io.EOF {
		return nil, errNotFound(filename)
	} else if err != nil {
		return nil, err
	} else if header.Typeflag != tar.TypeReg || header.Name != filename {
		return nil, errNotFound(filename)
	}
	content, err := io.ReadAll(tr)
	if err != nil {
		return nil, err
	}

	if err := zr.Close(); err != nil {
		return nil, err
	}
	return content, nil
}

func (p *dockerPuller) Reference() reference.Canonical {
	return p.reference
}

func (p *dockerPuller) ConfigJSON(ctx context.Context) ([]byte, error) {
	payload, err := GetBlob(ctx, "", p.repo, p.config)
	if err != nil {
		return nil, fmt.Errorf("failed to get the header: %w", err)
	}
	configJSON, err := p.extractTarball("crbox.json", payload)
	if err != nil {
		if _, ok := err.(errNotFound); ok {
			return nil, ErrNotCrbox
		}
		return nil, fmt.Errorf("invalid header: %w", err)
	}
	return configJSON, nil
}

func (p *dockerPuller) ReadChunk(ctx context.Context) ([]byte, error) {
	if p.chunkCount >= p.NumChunks() {
		return nil, io.EOF
	}
	desc := p.layers[p.chunkCount]
	p.chunkCount++

	action := fmt.Sprintf("Downloading Chunk #%d", p.chunkCount)
	payload, err := GetBlob(ctx, action, p.repo, desc)
	if err != nil {
		return nil, fmt.Errorf("failed to get chunk #%d: %w", p.chunkCount, err)
	}
	filename := fmt.Sprintf("%06d.dat", p.chunkCount)
	content, err := p.extractTarball(filename, payload)
	if err != nil {
		return nil, fmt.Errorf("invalid chunk #%d: %w", p.chunkCount, err)
	}
	return content, nil
}

func (p *dockerPuller) NumChunks() int {
	return len(p.layers)
}

var _ Pusher = (*dockerPusher)(nil)
var _ Puller = (*dockerPuller)(nil)
