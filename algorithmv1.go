// Copyright (c) 2021 cions
// Licensed under the MIT License. See LICENSE for details

package crbox

import (
	"crypto/cipher"
	"crypto/rand"
	"crypto/subtle"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"hash"
	"math/bits"
	"strings"
	"time"

	"golang.org/x/crypto/argon2"
	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/chacha20poly1305"
)

const saltSizeV1 = 16

type kdfParamsV1 struct {
	Time    uint32 `json:"t"`
	Memory  uint32 `json:"m"`
	Threads uint8  `json:"p"`
	Salt    []byte `json:"salt"`
}

type configV1 struct {
	Versioned
	KDFParams kdfParamsV1 `json:"kdfparams"`
	Nonce     []byte      `json:"nonce"`
	Filename  []byte      `json:"filename"`
	Filesize  uint64      `json:"filesize"`
	Checksum  string      `json:"checksum"`
	CreatedAt time.Time   `json:"created_at"`
}

type algorithmV1 struct {
	config     configV1
	aead       cipher.AEAD
	chunkCount uint64
	digester   hash.Hash
	filesize   uint64
}

type V1Options struct {
	Time    uint32
	Memory  uint32
	Threads uint8
}

func NewEncryptorV1(password []byte, filename []byte, opts *V1Options) (Encryptor, error) {
	salt := make([]byte, saltSizeV1)
	if _, err := rand.Read(salt); err != nil {
		return nil, err
	}

	key := argon2.IDKey(password, salt, opts.Time, opts.Memory, opts.Threads, chacha20poly1305.KeySize)
	aead, err := chacha20poly1305.NewX(key)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, chacha20poly1305.NonceSizeX)
	if _, err := rand.Read(nonce); err != nil {
		return nil, err
	}

	digester, err := blake2b.New512(nil)
	if err != nil {
		return nil, err
	}

	encryptedFilename := aead.Seal(nil, nonce, filename, nil)

	return &algorithmV1{
		config: configV1{
			Versioned: Versioned{Version: 1},
			KDFParams: kdfParamsV1{
				Time:    opts.Time,
				Memory:  opts.Memory,
				Threads: opts.Threads,
				Salt:    salt,
			},
			Nonce:     nonce,
			Filename:  encryptedFilename,
			CreatedAt: time.Now().UTC(),
		},
		aead:     aead,
		digester: digester,
	}, nil
}

func NewDecryptorV1(password []byte, configJSON []byte) (Decryptor, error) {
	var c configV1
	if err := json.Unmarshal(configJSON, &c); err != nil {
		return nil, fmt.Errorf("invalid header: %w", err)
	}

	var aead cipher.AEAD
	var err error
	if password != nil {
		p := c.KDFParams
		key := argon2.IDKey(password, p.Salt, p.Time, p.Memory, p.Threads, chacha20poly1305.KeySize)
		aead, err = chacha20poly1305.NewX(key)
		if err != nil {
			return nil, err
		}
	}

	if idx := strings.IndexByte(c.Checksum, ':'); idx < 0 {
		return nil, errors.New("invalid header: invalid checksum format")
	} else if c.Checksum[:idx] != "blake2b" {
		return nil, errors.New("unsupported checksum algorithm")
	}
	digester, err := blake2b.New512(nil)
	if err != nil {
		return nil, err
	}

	return &algorithmV1{
		config:   c,
		aead:     aead,
		digester: digester,
	}, nil
}

func addCounter(nonce []byte, counter uint64) []byte {
	w0 := binary.BigEndian.Uint64(nonce[16:24])
	w1 := binary.BigEndian.Uint64(nonce[8:16])
	w2 := binary.BigEndian.Uint64(nonce[0:8])

	w0, carry := bits.Add64(w0, counter, 0)
	w1, carry = bits.Add64(w1, 0, carry)
	w2, _ = bits.Add64(w2, 0, carry)

	sum := make([]byte, chacha20poly1305.NonceSizeX)
	binary.BigEndian.PutUint64(sum[16:24], w0)
	binary.BigEndian.PutUint64(sum[8:16], w1)
	binary.BigEndian.PutUint64(sum[0:8], w2)
	return sum
}

func (alg *algorithmV1) Filename() ([]byte, error) {
	if alg.aead == nil {
		return nil, errors.New("no password is provided")
	}
	filename, err := alg.aead.Open(nil, alg.config.Nonce, alg.config.Filename, nil)
	if err != nil {
		return nil, ErrInvalidTag
	}
	return filename, nil
}

func (alg *algorithmV1) Filesize() uint64 {
	return alg.config.Filesize
}

func (alg *algorithmV1) Checksum() string {
	return alg.config.Checksum
}

func (alg *algorithmV1) CreatedAt() time.Time {
	return alg.config.CreatedAt
}

func (alg *algorithmV1) Overhead() int {
	return alg.aead.Overhead()
}

func (alg *algorithmV1) Encrypt(dst, src []byte) []byte {
	alg.filesize += uint64(len(src))
	alg.digester.Write(src)

	alg.chunkCount++
	nonce := addCounter(alg.config.Nonce, alg.chunkCount)
	return alg.aead.Seal(dst, nonce, src, nil)
}

func (alg *algorithmV1) ConfigJSON() []byte {
	alg.config.Filesize = alg.filesize
	alg.config.Checksum = fmt.Sprintf("blake2b:%x", alg.digester.Sum(nil))

	serialized, err := json.Marshal(alg.config)
	if err != nil {
		panic(err)
	}
	return serialized
}

func (alg *algorithmV1) Decrypt(dst, src []byte) ([]byte, error) {
	if alg.aead == nil {
		return nil, errors.New("no password is provided")
	}

	alg.chunkCount++
	nonce := addCounter(alg.config.Nonce, alg.chunkCount)
	plaintext, err := alg.aead.Open(dst, nonce, src, nil)
	if err != nil {
		return nil, ErrInvalidTag
	}
	alg.filesize += uint64(len(plaintext))
	alg.digester.Write(plaintext)
	return plaintext, nil
}

func (alg *algorithmV1) Verify() error {
	checksum, err := hex.DecodeString(strings.TrimPrefix(alg.config.Checksum, "blake2b:"))
	if err != nil {
		return errors.New("invalid header: invalid checksum format")
	}
	if subtle.ConstantTimeCompare(alg.digester.Sum(nil), checksum) == 0 {
		return ErrInvalidChecksum
	}
	if alg.filesize != alg.config.Filesize {
		return ErrInvalidChecksum
	}
	return nil
}

var _ Encryptor = (*algorithmV1)(nil)
var _ Decryptor = (*algorithmV1)(nil)
