// Copyright (c) 2021 cions
// Licensed under the MIT License. See LICENSE for details

package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/cions/goenc/prompt"
	"github.com/distribution/distribution/v3/reference"
	"gitlab.com/cions/crbox"
)

const pullHelpMessage = `Usage: crbox pull [OPTIONS] NAME[:TAG|@DIGEST]

Pull an artifact from a registry

Options:
  -o, --output=FILE     Output an artifact to FILE
  -n, --no-clobber      Do not overwrite an existing file
  -r, --retries=N       Maximum number of attempts to input password (default: 3)
  -q, --quiet           Be quiet
  -h, --help            Show this help message and exit

Environment Variables:
  PASSWORD              Encryption password`

type pullOptions struct {
	Output    string `short:"o" long:"output"`
	NoClobber bool   `short:"n" long:"no-clobber"`
	Retries   uint8  `short:"r" long:"retries"`
	Quiet     bool   `short:"q" long:"quiet"`
}

func pullMain(args []string) error {
	opts := &pullOptions{
		Retries: 3,
	}
	posargs, err := parseArgs(opts, args)
	switch {
	case err == errHelp:
		fmt.Println(pullHelpMessage)
		return nil
	case err != nil:
		return err
	case len(posargs) < 1:
		return errors.New("require arguments")
	case len(posargs) > 1:
		return errors.New("too many arguments")
	}

	ref, err := reference.ParseNormalizedNamed(posargs[0])
	if err != nil {
		return err
	}
	ref = reference.TagNameOnly(ref)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	repo, err := crbox.NewRepository(ctx, ref, "pull")
	if err != nil {
		return err
	}

	ctx, cancel = context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	puller, err := crbox.NewPuller(ctx, repo, ref)
	if err != nil {
		return err
	}
	configJSON, err := puller.ConfigJSON(ctx)
	if err != nil {
		return err
	}

	var terminal *prompt.Terminal

	var decryptor crbox.Decryptor
	var filename []byte
	if value, ok := os.LookupEnv("PASSWORD"); ok {
		password := []byte(value)
		decryptor, err = crbox.NewDecryptor(password, configJSON)
		if err != nil {
			return err
		}
		filename, err = decryptor.Filename()
		if err != nil {
			return err
		}
	} else {
		terminal, err = prompt.NewTerminal()
		if err != nil {
			return err
		}
		defer terminal.Close()

		var tries uint8 = 1
		for {
			password, err := terminal.ReadPassword(context.Background(), "Password: ")
			if err != nil {
				return err
			}
			decryptor, err = crbox.NewDecryptor(password, configJSON)
			if err != nil {
				return err
			}
			filename, err = decryptor.Filename()
			if errors.Is(err, crbox.ErrInvalidTag) && tries < opts.Retries {
				fmt.Fprintf(terminal, "crbox: error: incorrect password. try again.\n")
				tries++
				continue
			} else if err != nil {
				return err
			} else {
				break
			}
		}
	}

	if !opts.Quiet {
		if terminal == nil {
			terminal, err = prompt.NewTerminal()
			if err == nil {
				defer terminal.Close()
			}
		}
		if terminal != nil {
			crbox.SetProgress(terminal)
			defer crbox.CloseProgress()
		}
	}

	var w io.Writer = os.Stdout
	if opts.Output != "-" {
		outfile := string(filename)
		if opts.Output != "" {
			outfile = opts.Output
		}
		flags := os.O_WRONLY | os.O_CREATE | os.O_TRUNC
		if opts.NoClobber {
			flags |= os.O_EXCL
		}
		fh, err := os.OpenFile(outfile, flags, 0o644)
		if err != nil {
			return err
		}
		defer fh.Close()
		w = fh
	}

	for i := 1; i <= puller.NumChunks(); i++ {
		ciphertext, err := puller.ReadChunk(context.Background())
		if err != nil {
			return err
		}
		plaintext, err := decryptor.Decrypt(ciphertext[:0], ciphertext)
		if err != nil {
			return err
		}
		if _, err := w.Write(plaintext); err != nil {
			return err
		}
	}

	if err := decryptor.Verify(); err != nil {
		return err
	}

	return nil
}
