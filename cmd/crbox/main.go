// Copyright (c) 2021 cions
// Licensed under the MIT License. See LICENSE for details

package main

import (
	"errors"
	"fmt"
	"os"
	"reflect"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"

	"github.com/cions/goenc/prompt"
	"gitlab.com/cions/crbox"
)

const helpMessage = `Usage: crbox COMMAND [ARGUMENTS...]

Container Registry as a cloud storage

Commands:
  push                  Push an artifact to a registry
  pull                  Pull an artifact from a registry
  copy                  Copy an artifact to another tags or registries
  list                  List artifacts in repositories

Options:
  -h, --help            Show this help message and exit
      --version         Show version information and exit`

var errHelp = errors.New("show help")

func main() {
	if len(os.Args) < 2 {
		fmt.Println(helpMessage)
		os.Exit(0)
	}

	var err error
	switch os.Args[1] {
	case "push":
		err = pushMain(os.Args[2:])
	case "pull":
		err = pullMain(os.Args[2:])
	case "copy":
		err = copyMain(os.Args[2:])
	case "list":
		err = listMain(os.Args[2:])
	case "help":
		var command string
		if len(os.Args) > 2 {
			command = os.Args[2]
		}
		switch command {
		case "push":
			fmt.Println(pushHelpMessage)
		case "pull":
			fmt.Println(pullHelpMessage)
		case "copy":
			fmt.Println(copyHelpMessage)
		case "list":
			fmt.Println(listHelpMessage)
		default:
			fmt.Println(helpMessage)
		}
	case "-h", "--help":
		fmt.Println(helpMessage)
	case "--version":
		version := "(devel)"
		if bi, ok := debug.ReadBuildInfo(); ok {
			version = strings.TrimPrefix(bi.Main.Version, "v")
		}
		fmt.Printf("crbox %s (%s/%s)\n", version, runtime.GOOS, runtime.GOARCH)
	default:
		if !strings.HasPrefix(os.Args[1], "-") || os.Args[1] == "-" || os.Args[1] == "--" {
			err = fmt.Errorf("unknown command '%s'", os.Args[1])
		} else if strings.HasPrefix(os.Args[1], "--") {
			name := os.Args[1]
			if idx := strings.IndexByte(name, '='); idx >= 0 {
				name = name[:idx]
			}
			err = fmt.Errorf("unknown option '%s'", name)
		} else {
			err = fmt.Errorf("unknown option '%s'", os.Args[1][:2])
		}
	}

	if err != nil {
		var se prompt.SignalError
		if errors.As(err, &se) {
			os.Exit(128 + se.Signal())
		}
		fmt.Fprintf(os.Stderr, "crbox: error: %v\n", err)
		if errors.Is(err, crbox.ErrInvalidTag) || errors.Is(err, crbox.ErrInvalidChecksum) {
			os.Exit(1)
		}
		os.Exit(2)
	}
}

func parseArgs(opts interface{}, args []string) (posargs []string, err error) {
	flagset := make(map[string]reflect.Value)
	format := make(map[string]string)

	ropts := reflect.ValueOf(opts).Elem()
	for i, n := 0, ropts.NumField(); i < n; i++ {
		field := ropts.Type().Field(i)
		value := ropts.Field(i)
		if short := field.Tag.Get("short"); short != "" {
			flagset["-"+short] = value
			if v := field.Tag.Get("format"); v != "" {
				format["-"+short] = v
			}
		}
		if long := field.Tag.Get("long"); long != "" {
			flagset["--"+long] = value
			if v := field.Tag.Get("format"); v != "" {
				format["--"+long] = v
			}
		}
	}

	for len(args) > 0 {
		var name, value string
		switch {
		case !strings.HasPrefix(args[0], "-"), args[0] == "-":
			posargs = append(posargs, args[0])
			args = args[1:]
			continue
		case args[0] == "--":
			posargs = append(posargs, args[1:]...)
			args = args[len(args):]
			continue
		case strings.HasPrefix(args[0], "--"):
			if idx := strings.IndexByte(args[0], '='); idx >= 0 {
				name = args[0][:idx]
				value = args[0][idx+1:]
				if v, ok := flagset[name]; ok && v.Kind() == reflect.Bool {
					return nil, fmt.Errorf("option %s takes no value", name)
				}
				args = args[1:]
			} else {
				name = args[0]
				if v, ok := flagset[name]; ok && v.Kind() != reflect.Bool {
					if len(args) == 1 {
						return nil, fmt.Errorf("option %s requires a value", name)
					}
					value = args[1]
					args = args[2:]
				} else {
					args = args[1:]
				}
			}
		default:
			name = args[0][:2]
			if len(args[0]) > 2 {
				if v, ok := flagset[name]; ok && v.Kind() != reflect.Bool {
					value = args[0][2:]
					args = args[1:]
				} else if ok && args[0][2] == '-' {
					return nil, fmt.Errorf("option %s takes no value", name)
				} else {
					args[0] = "-" + args[0][2:]
				}
			} else {
				if v, ok := flagset[name]; ok && v.Kind() != reflect.Bool {
					if len(args) == 1 {
						return nil, fmt.Errorf("option %s requires a value", name)
					}
					value = args[1]
					args = args[2:]
				} else {
					args = args[1:]
				}
			}
		}

		v, ok := flagset[name]
		if !ok {
			if name == "-h" || name == "--help" {
				return nil, errHelp
			}
			return nil, fmt.Errorf("unknown option '%s'", name)
		}
		switch v.Kind() {
		case reflect.Bool:
			v.SetBool(true)
		case reflect.String:
			v.SetString(value)
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			var unit uint64 = 1
			width := v.Type().Bits()
			switch format[name] {
			case "":
			case "bytes":
				if strings.HasSuffix(value, "k") {
					value = strings.TrimSuffix(value, "k")
					unit = 1024
					width -= 10
				} else if strings.HasSuffix(value, "M") {
					value = strings.TrimSuffix(value, "M")
					unit = 1024 * 1024
					width -= 20
				} else if strings.HasSuffix(value, "G") {
					value = strings.TrimSuffix(value, "G")
					unit = 1024 * 1024 * 1024
					width -= 30
				}
			case "kbytes":
				if strings.HasSuffix(value, "k") {
					value = strings.TrimSuffix(value, "k")
				} else if strings.HasSuffix(value, "M") {
					value = strings.TrimSuffix(value, "M")
					unit = 1024
					width -= 10
				} else if strings.HasSuffix(value, "G") {
					value = strings.TrimSuffix(value, "G")
					unit = 1024 * 1024
					width -= 20
				}
			default:
				panic("parseArgs: unsupported format")
			}
			num, err := strconv.ParseUint(value, 10, width)
			if err != nil {
				if errors.Is(err, strconv.ErrSyntax) && format[name] != "" {
					return nil, fmt.Errorf("option %s expects a number (with optional suffix k, M or G)", name)
				}
				if errors.Is(err, strconv.ErrSyntax) {
					return nil, fmt.Errorf("option %s expects a number", name)
				}
				if errors.Is(err, strconv.ErrRange) {
					return nil, fmt.Errorf("option %s: value out of range", name)
				}
				return nil, fmt.Errorf("option %s: %w", name, err)
			}
			v.SetUint(num * unit)
		default:
			panic("parseArgs: unsupported type")
		}
	}

	return posargs, nil
}
