// Copyright (c) 2021 cions
// Licensed under the MIT License. See LICENSE for details

package main

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	"github.com/cions/goenc/prompt"
	"gitlab.com/cions/crbox"
)

const pushHelpMessage = `Usage: crbox push [OPTIONS] NAME[:TAG]...

Push an artifact to a registry

Options:
  -i, --input=FILE      Read an artifact from the FILE rather than from STDIN
  -f, --filename=NAME   Use NAME as the name of an artifact
  -c, --chunk-size=N[kMG]
                        Maximum size of chunks (default: 256M)
  -t, --time=N          Argon2 time parameter (default: 8)
  -m, --memory=N[kMG]   Argon2 memory parameter (default: 1G)
  -p, --parallelism=N   Argon2 parallelism parameter (default: 4)
  -r, --retries=N       Maximum number of attempts to input password (default: 3)
  -q, --quiet           Be quiet
  -h, --help            Show this help message and exit

Environment Variables:
  PASSWORD              Encryption password`

type pushOptions struct {
	Input     string `short:"i" long:"input"`
	Filename  string `short:"f" long:"filename"`
	ChunkSize uint64 `short:"c" long:"chunk-size" format:"bytes"`
	Time      uint32 `short:"t" long:"time"`
	Memory    uint32 `short:"m" long:"memory" format:"kbytes"`
	Threads   uint8  `short:"p" long:"parallelism"`
	Retries   uint8  `short:"r" long:"retries"`
	Quiet     bool   `short:"q" long:"quiet"`
}

func pushMain(args []string) error {
	opts := &pushOptions{
		Input:     "-",
		ChunkSize: 256 * 1024 * 1024,
		Time:      8,
		Memory:    1 * 1024 * 1024,
		Threads:   4,
		Retries:   3,
	}
	posargs, err := parseArgs(opts, args)
	switch {
	case err == errHelp:
		fmt.Println(pushHelpMessage)
		return nil
	case err != nil:
		return err
	case len(posargs) < 1:
		return errors.New("require arguments")
	}

	var r io.Reader = os.Stdin
	if opts.Input != "-" {
		fh, err := os.Open(opts.Input)
		if err != nil {
			return err
		}
		defer fh.Close()
		r = fh
	}

	var terminal *prompt.Terminal

	var filename []byte
	if opts.Filename != "" {
		filename = []byte(opts.Filename)
	} else if opts.Input != "-" {
		filename = []byte(filepath.Base(opts.Input))
	} else {
		terminal, err = prompt.NewTerminal()
		if err != nil {
			return err
		}
		defer terminal.Close()

		filename, err = terminal.ReadString(context.Background(), "Filename: ")
		if err != nil {
			return err
		}
	}

	var password []byte
	if value, ok := os.LookupEnv("PASSWORD"); ok {
		password = []byte(value)
	} else {
		if terminal == nil {
			terminal, err = prompt.NewTerminal()
			if err != nil {
				return err
			}
			defer terminal.Close()
		}

		var tries uint8 = 1
		for {
			password, err = terminal.ReadPassword(context.Background(), "Password: ")
			if err != nil {
				return err
			}
			confirmPassword, err := terminal.ReadPassword(context.Background(), "Confirm Password: ")
			if err != nil {
				return err
			}
			if bytes.Equal(password, confirmPassword) {
				break
			} else if tries < opts.Retries {
				fmt.Fprintf(terminal, "crbox: error: passwords does not match. try again.\n")
				tries++
				continue
			} else {
				return errors.New("passwords does not match")
			}
		}
	}

	if !opts.Quiet {
		if terminal == nil {
			terminal, err = prompt.NewTerminal()
			if err == nil {
				defer terminal.Close()
			}
		}
		if terminal != nil {
			crbox.SetProgress(terminal)
			defer crbox.CloseProgress()
		}
	}

	encryptor, err := crbox.NewEncryptorV1(password, filename, &crbox.V1Options{
		Time:    opts.Time,
		Memory:  opts.Memory,
		Threads: opts.Threads,
	})
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	dst, err := crbox.NewPushDestinations(ctx, posargs)
	if err != nil {
		return err
	}

	pusher, err := crbox.NewDockerPusher(dst)
	if err != nil {
		return err
	}

	r = newChunkedReader(r)
	buffer := make([]byte, opts.ChunkSize+uint64(encryptor.Overhead()))[:opts.ChunkSize]
	for chunkCount := 1; ; chunkCount++ {
		n, err := r.Read(buffer)
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}
		ciphertext := encryptor.Encrypt(buffer[:0], buffer[:n])

		if err := pusher.PutChunk(context.Background(), ciphertext); err != nil {
			return fmt.Errorf("chunk #%d: %w", chunkCount, err)
		}
	}

	ctx, cancel = context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	if err := pusher.Commit(ctx, encryptor.ConfigJSON()); err != nil {
		return err
	}

	return nil
}

type chunkedReader struct {
	r   io.Reader
	err error
}

func newChunkedReader(r io.Reader) io.Reader {
	return &chunkedReader{r: r}
}

func (r *chunkedReader) Read(p []byte) (n int, err error) {
	if r.err != nil {
		return 0, r.err
	}
	for n < len(p) && err == nil {
		var nn int
		nn, err = r.r.Read(p[n:])
		n += nn
	}
	r.err = err
	if n == 0 {
		return 0, err
	}
	return n, nil
}
