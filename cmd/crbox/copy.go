// Copyright (c) 2021 cions
// Licensed under the MIT License. See LICENSE for details

package main

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/cions/goenc/prompt"
	"github.com/distribution/distribution/v3"
	"github.com/distribution/distribution/v3/manifest/schema2"
	"github.com/distribution/distribution/v3/reference"
	"github.com/opencontainers/go-digest"
	"gitlab.com/cions/crbox"
)

const copyHelpMessage = `Usage: crbox copy SOURCE[:TAG|@DIGEST] DEST[:TAG]...

Copy an artifact to another tags or repositories

Options:
  -q, --quiet           Be quiet
  -h, --help            Show this help message and exit`

type copyOptions struct {
	Quiet bool `short:"q" long:"quiet"`
}

func copyMain(args []string) error {
	opts := &copyOptions{}
	posargs, err := parseArgs(opts, args)
	switch {
	case err == errHelp:
		fmt.Println(copyHelpMessage)
		return nil
	case err != nil:
		return err
	case len(posargs) < 2:
		return fmt.Errorf("require two or more arguments")
	}

	if !opts.Quiet {
		if terminal, err := prompt.NewTerminal(); err == nil {
			crbox.SetProgress(terminal)
			defer terminal.Close()
			defer crbox.CloseProgress()
		}
	}

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	srcRepo, manifest, err := getManifest(ctx, posargs[0])
	if err != nil {
		return err
	}

	ctx, cancel = context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	destinations, err := crbox.NewPushDestinations(ctx, posargs[1:])
	if err != nil {
		return err
	}

	for _, desc := range manifest.References() {
		if err := copyBlob(srcRepo, destinations, desc); err != nil {
			return err
		}
	}

	ctx, cancel = context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	if err := crbox.PutManifests(ctx, destinations); err != nil {
		return err
	}

	return nil
}

func getManifest(ctx context.Context, src string) (distribution.Repository, distribution.Manifest, error) {
	ref, err := reference.ParseNormalizedNamed(src)
	if err != nil {
		return nil, nil, err
	}
	ref = reference.TagNameOnly(ref)

	repo, err := crbox.NewRepository(ctx, ref, "pull")
	if err != nil {
		return nil, nil, err
	}

	var dgst digest.Digest
	if digested, ok := ref.(reference.Digested); ok {
		dgst = digested.Digest()
	} else if tagged, ok := ref.(reference.Tagged); ok {
		desc, err := repo.Tags(ctx).Get(ctx, tagged.Tag())
		if err != nil {
			return nil, nil, fmt.Errorf("manifest for %s not found: %w", ref, err)
		}
		dgst = desc.Digest
	} else {
		return nil, nil, fmt.Errorf("reference must be tagged or digested")
	}

	ms, err := repo.Manifests(ctx)
	if err != nil {
		return nil, nil, err
	}
	manifest, err := ms.Get(ctx, dgst)
	if err != nil {
		return nil, nil, err
	}
	return repo, manifest, nil
}

func copyBlob(srcRepo distribution.Repository, destinations []*crbox.PushDestination, desc distribution.Descriptor) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	var transferDestinations []*crbox.PushDestination
	for _, dst := range destinations {
		dstDesc, err := dst.Repository.Blobs(ctx).Stat(ctx, desc.Digest)
		if errors.Is(err, distribution.ErrBlobUnknown) {
			transferDestinations = append(transferDestinations, dst)
			continue
		} else if err != nil {
			return err
		}
		dstDesc.MediaType = desc.MediaType
		if dstDesc.MediaType == schema2.MediaTypeImageConfig {
			dst.Config = dstDesc
		} else {
			dst.Layers = append(dst.Layers, dstDesc)
		}
	}
	if len(transferDestinations) == 0 {
		return nil
	}

	dgst := desc.Digest.Encoded()[:10]

	action := fmt.Sprintf("Downloading blob %s", dgst)
	payload, err := crbox.GetBlob(context.Background(), action, srcRepo, desc)
	if err != nil {
		return err
	}

	action = fmt.Sprintf("Uploading blob %s", dgst)
	descs, err := crbox.PutBlob(context.Background(), action, transferDestinations, desc.MediaType, payload)
	if err != nil {
		return err
	}
	for i, dst := range transferDestinations {
		dstDesc := descs[i]
		if dstDesc.MediaType == schema2.MediaTypeImageConfig {
			dst.Config = dstDesc
		} else {
			dst.Layers = append(dst.Layers, dstDesc)
		}
	}

	return nil
}
