// Copyright (c) 2021 cions
// Licensed under the MIT License. See LICENSE for details

package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"text/tabwriter"
	"text/template"
	"time"

	"github.com/cions/goenc/prompt"
	"github.com/distribution/distribution/v3"
	"github.com/distribution/distribution/v3/reference"
	"github.com/docker/go-units"
	"gitlab.com/cions/crbox"
	"golang.org/x/sync/semaphore"
)

const listHelpMessage = `Usage: crbox list [OPTIONS] NAME[:TAG|@DIGEST]...

List artifacts in repositories

Options:
  -f, --filename        Show filename
      --digests         Show digests
      --format=FORMAT   Format the output using the given Go template
  -j, --json            Show in JSON format
  -r, --retries=N       Maximum number of attempts to input password (default: 3)
  -h, --help            Show this help message and exit

Environment Variables:
  PASSWORD              Encryption password`

type listOptions struct {
	Filename bool   `short:"f" long:"filename"`
	Digests  bool   `long:"digests"`
	Format   string `long:"format"`
	JSON     bool   `short:"j" long:"json"`
	Retries  uint8  `short:"r" long:"retries"`
}

type listEntry struct {
	Repository   string
	Tag          string
	Digest       string
	Filename     string
	CreatedAt    string
	CreatedSince string
	Size         string
	HumanSize    string
	Checksum     string
}

func listMain(args []string) error {
	opts := &listOptions{
		Retries: 3,
	}
	posargs, err := parseArgs(opts, args)
	switch {
	case err == errHelp:
		fmt.Println(listHelpMessage)
		return nil
	case err != nil:
		return err
	case len(posargs) < 1:
		return errors.New("require arguments")
	}

	var pool *passwordPool
	if opts.Filename {
		pool = &passwordPool{Retries: opts.Retries}
		defer pool.Close()
	}

	lister := newLister(pool, 8)

	entries := flatMap(posargs, lister.getListEntries)

	var outputter func(*listOptions, []*listEntry) error
	switch {
	case opts.JSON:
		outputter = jsonOutputter
	case opts.Format != "":
		outputter = templateOutputter
	default:
		outputter = defaultOutputter
	}
	if err := outputter(opts, entries); err != nil {
		return err
	}

	return nil
}

func flatMap(a []string, f func(string) []*listEntry) []*listEntry {
	resultList := make([][]*listEntry, len(a))
	var wg sync.WaitGroup
	for i, x := range a {
		i, x := i, x
		wg.Add(1)
		go func() {
			defer wg.Done()
			resultList[i] = f(x)
		}()
	}
	wg.Wait()

	var result []*listEntry
	for _, xs := range resultList {
		result = append(result, xs...)
	}
	return result
}

type passwordPool struct {
	Retries uint8

	passwordset [][]byte
	pmu         sync.RWMutex

	terminal *prompt.Terminal
	tmu      sync.Mutex
}

func (p *passwordPool) tryKnownPasswords(configJSON []byte, start int) (crbox.Decryptor, int, error) {
	p.pmu.RLock()
	defer p.pmu.RUnlock()

	for _, password := range p.passwordset[start:] {
		decryptor, err := crbox.NewDecryptor(password, configJSON)
		if err != nil {
			return nil, -1, err
		}
		_, err = decryptor.Filename()
		if errors.Is(err, crbox.ErrInvalidTag) {
			continue
		} else if err != nil {
			return nil, -1, err
		}
		return decryptor, -1, nil
	}
	return nil, len(p.passwordset), nil
}

func (p *passwordPool) promptPassword(refName string, configJSON []byte) (crbox.Decryptor, error) {
	if p.terminal == nil {
		terminal, err := prompt.NewTerminal()
		if err != nil {
			return nil, err
		}
		p.terminal = terminal
	}

	promptText := fmt.Sprintf("Password for %s: ", refName)

	var tries uint8 = 1
	for {
		password, err := p.terminal.ReadPassword(context.Background(), promptText)
		if err != nil {
			return nil, err
		}
		decryptor, err := crbox.NewDecryptor(password, configJSON)
		if err != nil {
			return nil, err
		}
		_, err = decryptor.Filename()
		if errors.Is(err, crbox.ErrInvalidTag) && tries < p.Retries {
			fmt.Fprintln(p.terminal, "crbox: error: incorrect password. try again.")
			tries++
			continue
		}
		if err != nil {
			return nil, err
		}
		p.pmu.Lock()
		defer p.pmu.Unlock()
		p.passwordset = append(p.passwordset, password)
		return decryptor, nil
	}
}

func (p *passwordPool) NewDecryptor(refName string, configJSON []byte) (crbox.Decryptor, error) {
	if p == nil {
		return crbox.NewDecryptor(nil, configJSON)
	}

	if value, ok := os.LookupEnv("PASSWORD"); ok {
		password := []byte(value)
		decryptor, err := crbox.NewDecryptor(password, configJSON)
		if err != nil {
			return nil, err
		}
		if _, err := decryptor.Filename(); err != nil {
			return nil, err
		}
		return decryptor, nil
	}

	decryptor, proceed, err := p.tryKnownPasswords(configJSON, 0)
	if err != nil {
		return nil, err
	} else if proceed < 0 {
		return decryptor, nil
	}

	p.tmu.Lock()
	defer p.tmu.Unlock()

	decryptor, proceed, err = p.tryKnownPasswords(configJSON, proceed)
	if err != nil {
		return nil, err
	} else if proceed < 0 {
		return decryptor, nil
	}

	return p.promptPassword(refName, configJSON)
}

func (p *passwordPool) Close() {
	if p != nil && p.terminal != nil {
		p.terminal.Close()
	}
}

func (p *passwordPool) eprint(err error) {
	p.tmu.Lock()
	defer p.tmu.Unlock()

	fmt.Fprintf(os.Stderr, "crbox: error: %v\n", err)
}

type lister struct {
	pool *passwordPool
	sem  *semaphore.Weighted
}

func newLister(pool *passwordPool, n int64) *lister {
	return &lister{
		pool: pool,
		sem:  semaphore.NewWeighted(n),
	}
}

func (l *lister) eprint(err error) {
	if l.pool != nil {
		l.pool.eprint(err)
	} else {
		fmt.Fprintf(os.Stderr, "crbox: error: %v\n", err)
	}
}

func (l *lister) getListEntries(arg string) []*listEntry {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	ref, err := reference.ParseNormalizedNamed(arg)
	if err != nil {
		l.eprint(fmt.Errorf("%s: %w", arg, err))
		return nil
	}
	repoName := reference.FamiliarName(ref)

	repo, err := crbox.NewRepository(ctx, ref, "pull")
	if err != nil {
		l.eprint(fmt.Errorf("%s: %w", repoName, err))
		return nil
	}

	if !reference.IsNameOnly(ref) {
		return l.getListEntry(repo, ref)
	}

	tags, err := repo.Tags(ctx).All(ctx)
	if err != nil {
		l.eprint(fmt.Errorf("%s: %w", repoName, err))
		return nil
	}

	entries := flatMap(tags, func(tag string) []*listEntry {
		ref, err := reference.WithTag(ref, tag)
		if err != nil {
			l.eprint(fmt.Errorf("%s: %w", repoName, err))
			return nil
		}
		return l.getListEntry(repo, ref)
	})

	sort.SliceStable(entries, func(i, j int) bool {
		return strings.Compare(entries[i].CreatedAt, entries[j].CreatedAt) < 0
	})

	return entries
}

func (l *lister) getListEntry(repo distribution.Repository, ref reference.Named) []*listEntry {
	l.sem.Acquire(context.Background(), 1)
	defer l.sem.Release(1)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	refName := reference.FamiliarName(ref)
	if tagged, ok := ref.(reference.Tagged); ok {
		refName += ":" + tagged.Tag()
	} else if digested, ok := ref.(reference.Digested); ok {
		refName += "@" + digested.Digest().String()
	}

	puller, err := crbox.NewPuller(ctx, repo, ref)
	if err != nil {
		l.eprint(fmt.Errorf("%s: %w", refName, err))
		return nil
	}
	ref = puller.Reference()

	configJSON, err := puller.ConfigJSON(ctx)
	if err != nil {
		l.eprint(fmt.Errorf("%s: %w", refName, err))
		return nil
	}

	c, err := l.pool.NewDecryptor(refName, configJSON)
	var se prompt.SignalError
	if errors.As(err, &se) {
		os.Exit(128 + se.Signal())
	}
	if errors.Is(err, crbox.ErrInvalidTag) {
		c, err = crbox.NewDecryptor(nil, configJSON)
	}
	if err != nil {
		l.eprint(fmt.Errorf("%s: %w", refName, err))
		return nil
	}

	entry := &listEntry{}
	entry.Repository = reference.FamiliarName(ref)
	if tagged, ok := ref.(reference.Tagged); ok {
		entry.Tag = tagged.Tag()
	}
	if digested, ok := ref.(reference.Digested); ok {
		entry.Digest = digested.Digest().String()
	}
	entry.Checksum = c.Checksum()
	entry.CreatedAt = c.CreatedAt().Format(time.RFC3339)
	entry.CreatedSince = units.HumanDuration(time.Since(c.CreatedAt()))
	entry.Size = strconv.FormatUint(c.Filesize(), 10)
	entry.HumanSize = units.BytesSize(float64(c.Filesize()))
	if filename, err := c.Filename(); err == nil {
		entry.Filename = string(filename)
	}
	return []*listEntry{entry}
}

func jsonOutputter(opts *listOptions, entries []*listEntry) error {
	enc := json.NewEncoder(os.Stdout)
	enc.SetEscapeHTML(false)
	enc.SetIndent("", "  ")
	if err := enc.Encode(entries); err != nil {
		return err
	}
	return nil
}

func templateOutputter(opts *listOptions, entries []*listEntry) error {
	tmpl, err := template.New("root").Parse(opts.Format)
	if err != nil {
		return err
	}
	for _, entry := range entries {
		if err := tmpl.Execute(os.Stdout, entry); err != nil {
			return err
		}
		if _, err := fmt.Println(); err != nil {
			return err
		}
	}
	return nil
}

func defaultOutputter(opts *listOptions, entries []*listEntry) error {
	if len(entries) == 0 {
		return nil
	}

	w := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0)
	columns := make([]string, 0, 6)

	columns = append(columns, "REPOSITORY", "TAG")
	if opts.Digests {
		columns = append(columns, "DIGEST")
	}
	if opts.Filename {
		columns = append(columns, "FILENAME")
	}
	columns = append(columns, "CREATED", "SIZE")
	if _, err := fmt.Fprintln(w, strings.Join(columns, "\t")); err != nil {
		return err
	}

	for _, entry := range entries {
		columns = append(columns[:0], entry.Repository, entry.Tag)
		if opts.Digests {
			columns = append(columns, entry.Digest)
		}
		if opts.Filename {
			columns = append(columns, entry.Filename)
		}
		columns = append(columns, entry.CreatedSince, entry.HumanSize)
		if _, err := fmt.Fprintln(w, strings.Join(columns, "\t")); err != nil {
			return err
		}
	}
	if err := w.Flush(); err != nil {
		return err
	}

	return nil
}
