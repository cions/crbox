// Copyright (c) 2021 cions
// Licensed under the MIT License. See LICENSE for details

package crbox

import (
	"context"
	"fmt"
	"io"
	"os/signal"
	"strings"
	"sync"
	"syscall"

	"github.com/distribution/distribution/v3"
	"github.com/distribution/distribution/v3/reference"
	"github.com/docker/go-units"
	"github.com/opencontainers/go-digest"
	"golang.org/x/sync/errgroup"
)

const (
	clear_line       = "\r\x1b[K"
	cursor_invisible = "\x1b[?25l"
	cursor_normal    = "\x1b[?25h"
)

var progress *progressCtx

type progressCtx struct {
	mu      sync.Mutex
	w       io.Writer
	numbers map[string]int
	current int
}

func (p *progressCtx) printf(number int, format string, a ...interface{}) {
	if p == nil {
		return
	}

	var move_cursor string
	if diff := number - p.current; diff > 0 {
		move_cursor = strings.Repeat("\n", diff)
	} else if diff < 0 {
		move_cursor = fmt.Sprintf("\x1b[%dA", -diff)
	}
	p.current = number
	fmt.Fprintf(p.w, move_cursor+clear_line+format, a...)
}

func (p *progressCtx) update(action string, current, total int) {
	if p == nil || action == "" {
		return
	}

	p.mu.Lock()
	defer p.mu.Unlock()

	number, ok := p.numbers[action]
	if !ok {
		number = len(p.numbers)
		p.numbers[action] = number
	}
	p.printf(number, "%s: %8s/%8s", action, units.BytesSize(float64(current)), units.BytesSize(float64(total)))
}

func (p *progressCtx) Close() {
	if p == nil {
		return
	}

	p.mu.Lock()
	defer p.mu.Unlock()

	p.printf(len(p.numbers), "%s", cursor_normal)
}

func SetProgress(w io.Writer) {
	progress = &progressCtx{
		w:       w,
		numbers: make(map[string]int),
	}
	io.WriteString(progress.w, cursor_invisible)
}

func CloseProgress() {
	progress.Close()
}

func GetBlob(ctx context.Context, action string, repo distribution.Repository, desc distribution.Descriptor) (payload []byte, err error) {
	ctx, cancel := signal.NotifyContext(ctx, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)
	defer cancel()

	go progress.update(action, 0, int(desc.Size))

	r, err := repo.Blobs(ctx).Open(ctx, desc.Digest)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	n := 0
	payload = make([]byte, desc.Size)
	for n < len(payload) {
		nn, err := r.Read(payload[n:])
		n += nn
		go progress.update(action, n, int(desc.Size))
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}
	}
	if err := r.Close(); err != nil {
		return nil, err
	}
	return payload, nil
}

type reader struct {
	ctx     context.Context
	action  string
	payload []byte
	offset  int
}

func newReader(ctx context.Context, action string, payload []byte) io.Reader {
	return &reader{
		ctx:     ctx,
		action:  action,
		payload: payload,
	}
}

func (r *reader) Read(p []byte) (n int, err error) {
	select {
	case <-r.ctx.Done():
		return 0, r.ctx.Err()
	default:
	}

	go progress.update(r.action, r.offset, len(r.payload))
	if r.offset >= len(r.payload) {
		return 0, io.EOF
	}
	n = copy(p, r.payload[r.offset:])
	r.offset += n
	return n, nil
}

func PutBlob(ctx context.Context, action string, destinations []*PushDestination, mediaType string, payload []byte) (descs []distribution.Descriptor, err error) {
	ctx, cancel := signal.NotifyContext(ctx, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)
	defer cancel()

	provisional := distribution.Descriptor{
		MediaType: mediaType,
		Size:      int64(len(payload)),
		Digest:    digest.SHA256.FromBytes(payload),
	}

	descs = make([]distribution.Descriptor, len(destinations))
	g, ctx := errgroup.WithContext(ctx)
	for i, dst := range destinations {
		i, dst := i, dst
		repoName := reference.FamiliarName(dst.Name)
		subaction := action
		if action != "" && len(destinations) > 1 {
			subaction = fmt.Sprintf("%s to %s", action, repoName)
		}
		progress.update(subaction, 0, len(payload))
		g.Go(func() (err error) {
			w, err := dst.Repository.Blobs(ctx).Create(ctx)
			if err != nil {
				return fmt.Errorf("%s: %w", repoName, err)
			}
			defer w.Cancel(context.Background())

			r := newReader(ctx, subaction, payload)
			n := 0
			for n < len(payload) {
				nn, err := w.ReadFrom(r)
				n += int(nn)
				go progress.update(subaction, n, len(payload))
				if err != nil {
					return fmt.Errorf("%s: %w", repoName, err)
				}
			}

			desc, err := w.Commit(ctx, provisional)
			if err != nil {
				return fmt.Errorf("%s: %w", repoName, err)
			}
			desc.MediaType = mediaType
			descs[i] = desc
			return nil
		})
	}
	if err := g.Wait(); err != nil {
		return nil, err
	}
	return descs, nil
}
